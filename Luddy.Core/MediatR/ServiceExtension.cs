﻿using System.Reflection;
using Autofac;
using MediatR.Extensions.Autofac.DependencyInjection;

namespace Luddy.Core {
    public static partial class ServiceExtension {
        public static ContainerBuilder BuildMediatR (this ContainerBuilder builder, params Assembly[] assemblys) {
            builder.AddMediatR (assemblys);
            return builder;
        }
    }
}