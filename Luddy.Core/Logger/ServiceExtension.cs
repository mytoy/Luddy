﻿using Autofac;
using AutofacSerilogIntegration;
using Serilog;
using Serilog.Events;

namespace Luddy.Core {
    public static partial class ServiceExtension {
        public static ContainerBuilder BuildSerilog (this ContainerBuilder builder) {
            Log.Logger = new LoggerConfiguration ()
                .Enrich.FromLogContext ()
                .WriteTo
                .Console (LogEventLevel.Verbose, "[{Timestamp:yyyy-mm-dd HH:mm:ss.FFF} {Level}] {Message}{NewLine}{Exception}")
                .CreateLogger ();
            builder.RegisterLogger (autowireProperties: true);
            return builder;
        }
    }
}