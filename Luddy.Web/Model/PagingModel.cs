﻿using Luddy.Core.Entity;
using System.Collections;

namespace Luddy.Web.Model
{
    public class PagingModel<T> : PagingEntity where T : IEnumerable
    {
        public long TotalCount { get; set; }

        public T List { get; set; }
    }
}
