﻿using FluentValidation;
using FluentValidation.Results;
using Luddy.Web.Api;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Collections.Generic;
using System.Linq;

namespace Luddy.Web.Valid
{
    public class ValidateResponseAttribute : ActionFilterAttribute
    {
        private IValidatorFactory _validatorFactory;

        public ValidateResponseAttribute(IValidatorFactory validatorFactory)
        {
            _validatorFactory = validatorFactory;
        }

        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            foreach (KeyValuePair<string, object> arg in actionContext.ActionArguments)
            {
                if (arg.Value == null)
                {
                    break;
                }
                IValidator valid = _validatorFactory.GetValidator(arg.Value.GetType());
                ValidationResult result = valid?.Validate(arg.Value);
                if (result != null && !result.IsValid)
                {
                    MessageResult messageResult = new MessageResult { Status = false, Msg = string.Join(",", result.Errors.Select(o => o.ErrorMessage)) };
                    actionContext.Result = new JsonResult(messageResult);
                    break;
                }
            }
        }
    }
}
