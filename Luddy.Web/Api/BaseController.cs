﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using AutoMapper;
using Luddy.Core.Cache;
using Microsoft.AspNetCore.Mvc;

namespace Luddy.Web.Api {
    [Route ("api/v{version:apiVersion}/[controller]/[action]")]
    public abstract class BaseController : ControllerBase {
        /// <summary>
        /// 格式化器
        /// </summary>
        public IMapper _mapper { protected get; set; }
        public ConcurrentDictionary<string, string> ConStrings { private get; set; }

        protected RedisRepository GetRedis (string name = Core.Config.DEFAULT_REDIS_CONNECTION) {
            if (ConStrings != null && ConStrings.TryGetValue (name, out var con)) {
                return new RedisRepository (con);
            } else {
                return default;
            }
        }

        protected long GetUserID (string key = Config.DEFAULT_CLAIM_USERID) {
            return Convert.ToInt64 (HttpContext.User.Claims.First (o => o.Type == key).Value.First ());
        }
    }
}