using System.Reflection;
using Autofac;
using Luddy.Core;
using Luddy.Web;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Luddy.Test {
    public class Startup {
        public Startup (IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) {
            services.AddJsonAndVersion ();
        }

        public void ConfigureContainer (ContainerBuilder builder) {
            builder.BuildData (Configuration, "Luddy.Oper")
                .BuildWeb (Assembly.GetExecutingAssembly ())
                .BuildSerilog ().BuildCacheManager (new RedisConfig {
                    Database = 1,
                        Endpoints = "192.168.2.152",
                        Password = "myname"
                }, true);
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
            }
            //app.UseCors(builder => builder
            //    .AllowAnyOrigin()
            //    .AllowAnyMethod()
            //    .AllowAnyHeader()
            //    .AllowCredentials());

            app.UseRouting ();

            app.UseAuthorization ();

            app.UseEndpoints (endpoints => {
                endpoints.MapControllers ();
            });
        }
    }
}