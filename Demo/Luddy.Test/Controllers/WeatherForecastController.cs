﻿using System;
using System.Collections.Generic;
using System.Linq;
using CacheManager.Core;
using CacheManager.Core.Internal;
using Luddy.Core;
using Luddy.Oper;
using Luddy.Web.Api;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace Luddy.Test.Controllers {
    [ApiController]
    public class WeatherForecastController : BaseController {
        private static readonly string[] Summaries = new [] {
            "Freezing",
            "Bracing",
            "Chilly",
            "Cool",
            "Mild",
            "Warm",
            "Balmy",
            "Hot",
            "Sweltering",
            "Scorching"
        };
        private readonly IHomeRepository _homeRepository;
        private readonly ILogger _logger;
        private readonly ICacheManager<string> _cache;
        //private readonly IMediator _mediator;
        public WeatherForecastController (IHomeRepository homeRepository, ILogger logger, ICacheManager<string> cache) {
            _homeRepository = homeRepository;
            _logger = logger;
            _cache = cache;
            //_mediator = mediator;
        }

        [HttpGet]
        public string MyLog2 () {
            
            return _cache.Get ("lala");
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> MyLog1 () {
            var rng = new Random ();
            _cache.Put ("lala", rng.Next (-20, 55).ToString ());
            
            //_cache.OnGet += new EventHandler<CacheActionEventArgs> (MyHandler);
            //_cache["lala"] = rng.Next (-20, 55).ToString ();
            //_cache.Expire ("lala", ExpirationMode.Absolute, TimeSpan.FromSeconds (1));
            _logger.Information ("{classname}测试", this);
            return Enumerable.Range (1, 5).Select (index => new WeatherForecast {
                    Date = DateTime.Now.AddDays (index),
                        TemperatureC = rng.Next (-20, 55),
                        Summary = Summaries[rng.Next (Summaries.Length)],
                        Count = _homeRepository.GetUserCount ()
                })
                .ToArray ();

        }

        //private void MyHandler (object sender, CacheActionEventArgs args) {
        //    var main = (ICacheManager<string>) sender;
        //    //_logger.Information (main.Get (args.Key));
        //}
    }
}