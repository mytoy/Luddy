﻿using Dapper;
using Luddy.Core.Data;
using Microsoft.Extensions.Configuration;

namespace Luddy.Oper
{
    public class HomeRepository : BaseRepository, IHomeRepository
    {
        public ITestRepository _testRepository;
        public HomeRepository(ITestRepository testRepository)
        {
            _testRepository = testRepository;
        }
        public long GetUserCount()
        {
            //_testRepository.GetStockPlan();
            string sql = "select count(1) from user";
            return Conn.ExecuteScalar<long>(sql);
        }
    }
    public class TestRepository : BaseRepository, ITestRepository
    {
        public long GetStockPlan()
        {
            string sql = "select count(1) from stock_plan";
            return Conn.ExecuteScalar<long>(sql);
        }

    }
    public interface IHomeRepository
    {
        long GetUserCount();
    }
    public interface ITestRepository
    {
        long GetStockPlan();
    }
}
